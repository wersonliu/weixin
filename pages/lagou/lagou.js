Page({
  data: {
    lagous:[],
    next:false,
    next_page:[]
  },
  onLoad:function(){
    var that=this
    wx.request({
      url: 'http://127.0.0.1:8000/lagou/?format=json&page=1', //仅为示例，并非真实的接口地址
      method:'get',
      header: {
        'content-type': 'application/json' // 默认值
      },
      success(res) {
        that.setData({
          lagous:res.data.results
        })
        if(res.data.next){
          that.setData({
            next: true,
            next_page: res.data.next
          })
        }
      }
    })
  },
  viewmore:function(){
      if(this.data.next){
        var that = this
        wx.request({
          url: this.data.next_page, //仅为示例，并非真实的接口地址
          method: 'get',
          header: {
            'content-type': 'application/json' // 默认值
          },
          success(res) {
            that.setData({
              lagous: res.data.results
            })
            if (res.data.next) {
              that.setData({
                next: true,
                next_page: res.data.next
              })
            }
          }
        })
        
      }else{
        wx.showToast({
          title: '没有下一页',
          icon: 'success',
          duration: 2000
        })
      }
  }
})
